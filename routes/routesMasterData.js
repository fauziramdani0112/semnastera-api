var express = require('express');
var MasterDataCtrl = require('../controllers/MasterData.controller')
var auth = require('../tools/middleware');
var router = express.Router();

router.get('/auth', MasterDataCtrl.get_Auth)
router.get('/payment', MasterDataCtrl.get_Payment)
router.get('/files', MasterDataCtrl.get_Files)
router.get('/review', MasterDataCtrl.get_Review)

module.exports = router;