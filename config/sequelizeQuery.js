var Sequelize = require("sequelize");

const sequelizeSemnas = new Sequelize(process.env.semnas_database_name, process.env.semnas_database_user, process.env.semnas_database_password, {
    host: process.env.semnas_database_host,
    dialect: process.env.semnas_database_dialect,
    port: process.env.semnas_database_port,
    define: {
        timestamps: false,
        timezone: "+07:00"
    },
    logging: false,
    timezone: "+07:00",
    operatorsAliases: 0
});

sequelizeSemnas
.authenticate()
.then(() => {
  console.log('[OK] SEMNAS connected!');
})
.catch(err => {
  console.error('[ERR] SEMNAS connection error!', err);
});


module.exports = {
  sequelizeSemnas
};