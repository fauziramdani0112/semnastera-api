var fs = require("fs");
var path = require("path");
var Sequelize = require("sequelize");
var config = require("./config");

const Model_Semnas = path.join(__dirname, "../models/semnas");
const Config_Semnas = new Sequelize(
    config.semnas_db.name,
    config.semnas_db.username,
    config.semnas_db.password, {
    logging: config.semnas_db.logging,
    dialect: config.semnas_db.dialect,
    host: config.semnas_db.host,
    port: config.semnas_db.port,
    define: {
        timestamps: false,
        timezone: "+07:00"
    },
    timezone: "+07:00",
    operatorsAliases: 0
}
);
const db = {};
let model;

// ideabox Apps
fs.readdirSync(Model_Semnas)
    .filter(file => {
        return file.indexOf(".") !== 0 && file.indexOf(".map") === -1;
    })
    .forEach(file => {
        model = require(path.join(Model_Semnas, file))(Config_Semnas, Sequelize.DataTypes);
        db[model.name] = model;
    });
Object.keys(db).forEach(name => {
    if ("associate" in db[name]) {
        db[name].associate(db);
    }
});

module.exports = db