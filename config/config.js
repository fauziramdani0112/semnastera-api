module.exports = {
    semnas_db: {
        name: process.env.semnas_database_name,
        username: process.env.semnas_database_user,
        password: process.env.semnas_database_password,
        host: process.env.semnas_database_host,
        dialect: process.env.semnas_database_dialect,
        port: process.env.semnas_database_port,
        logging: false,

    },
    server: {
        port: process.env.Port
    },
    security: {
        salt: process.env.semnas_salt

    }
}