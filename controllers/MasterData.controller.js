var models = require('../config/sequelizeORM')
var sequelizeQuery = require('../config/sequelizeQuery')
var sequelizeORM = require('../config/sequelizeORM')
var api = require('../tools/common')
var moment = require('moment')
var btoa = require('btoa');
var formidable = require('formidable');
var mv = require('mv');
var path = require('path');
var cron = require('node-cron');

function get_Auth(req, res) {
    sequelizeQuery.sequelizeSemnas.query(
      `SELECT * FROM mst_authorization`, {
      type: sequelizeQuery.sequelizeSemnas.QueryTypes.SELECT
    }
    )
      .then(function (data) {
        if (data.length > 0) {
          api.ok(res, data)
        } else {
          api.error(res, 'Record not found', 200)
        }
      }).catch((e) => {
        console.log(e)
        api.error(res, e, 500)
      })
  }
function get_Files(req, res) {
    sequelizeQuery.sequelizeSemnas.query(
      `SELECT * FROM tr_files`, {
      type: sequelizeQuery.sequelizeSemnas.QueryTypes.SELECT
    }
    )
      .then(function (data) {
        if (data.length > 0) {
          api.ok(res, data)
        } else {
          api.error(res, 'Record not found', 200)
        }
      }).catch((e) => {
        console.log(e)
        api.error(res, e, 500)
      })
  }
function get_Payment(req, res) {
    sequelizeQuery.sequelizeSemnas.query(
      `SELECT * FROM tr_payment`, {
      type: sequelizeQuery.sequelizeSemnas.QueryTypes.SELECT
    }
    )
      .then(function (data) {
        if (data.length > 0) {
          api.ok(res, data)
        } else {
          api.error(res, 'Record not found', 200)
        }
      }).catch((e) => {
        console.log(e)
        api.error(res, e, 500)
      })
  }
function get_Review(req, res) {
    sequelizeQuery.sequelizeSemnas.query(
      `SELECT * FROM tr_review`, {
      type: sequelizeQuery.sequelizeSemnas.QueryTypes.SELECT
    }
    )
      .then(function (data) {
        if (data.length > 0) {
          api.ok(res, data)
        } else {
          api.error(res, 'Record not found', 200)
        }
      }).catch((e) => {
        console.log(e)
        api.error(res, e, 500)
      })
  }

module.exports = {
    get_Auth,
    get_Files,
    get_Payment,
    get_Review
};