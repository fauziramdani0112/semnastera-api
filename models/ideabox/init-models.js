var DataTypes = require("sequelize").DataTypes;
var _mst_approval_level = require("./mst_approval_level");
var _mst_area_implementation = require("./mst_area_implementation");
var _mst_area_implementation_1 = require("./mst_area_implementation_1");
var _mst_area_implementation_2 = require("./mst_area_implementation_2");
var _mst_area_type = require("./mst_area_type");
var _mst_aspect = require("./mst_aspect");
var _mst_authorization = require("./mst_authorization");
var _mst_configuration = require("./mst_configuration");
var _mst_department = require("./mst_department");
var _mst_department_head = require("./mst_department_head");
var _mst_grade = require("./mst_grade");
var _mst_hierarchy = require("./mst_hierarchy");
var _mst_improvement_type = require("./mst_improvement_type");
var _mst_job_grade = require("./mst_job_grade");
var _mst_job_owner = require("./mst_job_owner");
var _mst_level = require("./mst_level");
var _mst_map_department_aspect = require("./mst_map_department_aspect");
var _mst_point_bsc = require("./mst_point_bsc");
var _mst_reward = require("./mst_reward");
var _mst_reward_exchange_conf = require("./mst_reward_exchange_conf");
var _mst_role = require("./mst_role");
var _mst_target = require("./mst_target");
var _mst_technical_related = require("./mst_technical_related");
var _mst_value_aspect = require("./mst_value_aspect");
var _tbl_document_approver = require("./tbl_document_approver");
var _tbl_idea = require("./tbl_idea");
var _tbl_idea_history = require("./tbl_idea_history");
var _tbl_implementation = require("./tbl_implementation");
var _tbl_judging = require("./tbl_judging");
var _tbl_point = require("./tbl_point");
var _tbl_point_log = require("./tbl_point_log");
var _tbl_realease_score = require("./tbl_realease_score");
var _tbl_reward = require("./tbl_reward");
var _tbl_reward_cart = require("./tbl_reward_cart");
var _tbl_reward_log = require("./tbl_reward_log");
var _tbl_score = require("./tbl_score");
var _tbl_tasklist_approver = require("./tbl_tasklist_approver");
var _mst_candidate = require("./mst_candidate");

function initModels(sequelize) {
  var mst_approval_level = _mst_approval_level(sequelize, DataTypes);
  var mst_area_implementation = _mst_area_implementation(sequelize, DataTypes);
  var mst_area_implementation_1 = _mst_area_implementation_1(sequelize, DataTypes);
  var mst_area_implementation_2 = _mst_area_implementation_2(sequelize, DataTypes);
  var mst_area_type = _mst_area_type(sequelize, DataTypes);
  var mst_aspect = _mst_aspect(sequelize, DataTypes);
  var mst_authorization = _mst_authorization(sequelize, DataTypes);
  var mst_configuration = _mst_configuration(sequelize, DataTypes);
  var mst_department = _mst_department(sequelize, DataTypes);
  var mst_department_head = _mst_department_head(sequelize, DataTypes);
  var mst_grade = _mst_grade(sequelize, DataTypes);
  var mst_hierarchy = _mst_hierarchy(sequelize, DataTypes);
  var mst_improvement_type = _mst_improvement_type(sequelize, DataTypes);
  var mst_job_grade = _mst_job_grade(sequelize, DataTypes);
  var mst_job_owner = _mst_job_owner(sequelize, DataTypes);
  var mst_level = _mst_level(sequelize, DataTypes);
  var mst_map_department_aspect = _mst_map_department_aspect(sequelize, DataTypes);
  var mst_point_bsc = _mst_point_bsc(sequelize, DataTypes);
  var mst_reward = _mst_reward(sequelize, DataTypes);
  var mst_reward_exchange_conf = _mst_reward_exchange_conf(sequelize, DataTypes);
  var mst_role = _mst_role(sequelize, DataTypes);
  var mst_target = _mst_target(sequelize, DataTypes);
  var mst_technical_related = _mst_technical_related(sequelize, DataTypes);
  var mst_value_aspect = _mst_value_aspect(sequelize, DataTypes);
  var tbl_document_approver = _tbl_document_approver(sequelize, DataTypes);
  var tbl_idea = _tbl_idea(sequelize, DataTypes);
  var tbl_idea_history = _tbl_idea_history(sequelize, DataTypes);
  var tbl_implementation = _tbl_implementation(sequelize, DataTypes);
  var tbl_judging = _tbl_judging(sequelize, DataTypes);
  var tbl_point = _tbl_point(sequelize, DataTypes);
  var tbl_point_log = _tbl_point_log(sequelize, DataTypes);
  var tbl_realease_score = _tbl_realease_score(sequelize, DataTypes);
  var tbl_reward = _tbl_reward(sequelize, DataTypes);
  var tbl_reward_cart = _tbl_reward_cart(sequelize, DataTypes);
  var tbl_reward_log = _tbl_reward_log(sequelize, DataTypes);
  var tbl_score = _tbl_score(sequelize, DataTypes);
  var tbl_tasklist_approver = _tbl_tasklist_approver(sequelize, DataTypes);
  var mst_candidate = _mst_candidate(sequelize, DataTypes);


  return {
    mst_approval_level,
    mst_area_implementation,
    mst_area_implementation_1,
    mst_area_implementation_2,
    mst_area_type,
    mst_aspect,
    mst_authorization,
    mst_configuration,
    mst_department,
    mst_department_head,
    mst_grade,
    mst_hierarchy,
    mst_improvement_type,
    mst_job_grade,
    mst_job_owner,
    mst_level,
    mst_map_department_aspect,
    mst_point_bsc,
    mst_reward,
    mst_reward_exchange_conf,
    mst_role,
    mst_target,
    mst_technical_related,
    mst_value_aspect,
    tbl_document_approver,
    tbl_idea,
    tbl_idea_history,
    tbl_implementation,
    tbl_judging,
    tbl_point,
    tbl_point_log,
    tbl_realease_score,
    tbl_reward,
    tbl_reward_cart,
    tbl_reward_log,
    tbl_score,
    tbl_tasklist_approver,
    mst_candidate
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
