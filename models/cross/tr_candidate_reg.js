const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tr_candidate_reg', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    full_name: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    phone_number: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    password: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    birth_date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    gender: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    marital_status: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    religion: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    ethnic: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    education: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    institution: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    major: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    is_abroad: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    score: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    is_fresh_graduate: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    business_line: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    job_level: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    position: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    current_salary: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    expected_salary: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    job_function: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    city: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    experience_company: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    experience_position: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    experience_business_line: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    experience_start_date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    experience_end_date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    is_currently_working: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    skill: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    skill_rate: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    is_agree: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    pict: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    no_ktp: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    cv: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    npwp: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    prefer_location: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    sim_a: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    sim_c: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    reference: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    emergency_contact: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    organization: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    hobby: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'tr_candidate_reg',
    timestamps: true,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
