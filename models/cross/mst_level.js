const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('mst_level', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    company: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    level_code: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    level_description: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    is_aktif: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 1
    },
    last_update: {
      type: DataTypes.DATE,
      allowNull: true
    },
    update_by: {
      type: DataTypes.STRING(100),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'mst_level',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
