var DataTypes = require("sequelize").DataTypes;
var _mst_authorization = require("./mst_authorization");
var _mst_business_type = require("./mst_business_type");
var _mst_city = require("./mst_city");
var _mst_company = require("./mst_company");
var _mst_department = require("./mst_department");
var _mst_education = require("./mst_education");
var _mst_ethnic = require("./mst_ethnic");
var _mst_job_function = require("./mst_job_function");
var _mst_level = require("./mst_level");
var _mst_site = require("./mst_site");
var _mst_status = require("./mst_status");
var _tr_candidate_list = require("./tr_candidate_list");
var _tr_candidate_reg = require("./tr_candidate_reg");
var _tr_job_requisition = require("./tr_job_requisition");
var _tr_send_approval = require("./tr_send_approval");
var _tr_to_review = require("./tr_to_review");

function initModels(sequelize) {
  var mst_authorization = _mst_authorization(sequelize, DataTypes);
  var mst_business_type = _mst_business_type(sequelize, DataTypes);
  var mst_city = _mst_city(sequelize, DataTypes);
  var mst_company = _mst_company(sequelize, DataTypes);
  var mst_department = _mst_department(sequelize, DataTypes);
  var mst_education = _mst_education(sequelize, DataTypes);
  var mst_ethnic = _mst_ethnic(sequelize, DataTypes);
  var mst_job_function = _mst_job_function(sequelize, DataTypes);
  var mst_level = _mst_level(sequelize, DataTypes);
  var mst_site = _mst_site(sequelize, DataTypes);
  var mst_status = _mst_status(sequelize, DataTypes);
  var tr_candidate_list = _tr_candidate_list(sequelize, DataTypes);
  var tr_candidate_reg = _tr_candidate_reg(sequelize, DataTypes);
  var tr_job_requisition = _tr_job_requisition(sequelize, DataTypes);
  var tr_send_approval = _tr_send_approval(sequelize, DataTypes);
  var tr_to_review = _tr_to_review(sequelize, DataTypes);


  return {
    mst_authorization,
    mst_business_type,
    mst_city,
    mst_company,
    mst_department,
    mst_education,
    mst_ethnic,
    mst_job_function,
    mst_level,
    mst_site,
    mst_status,
    tr_candidate_list,
    tr_candidate_reg,
    tr_job_requisition,
    tr_send_approval,
    tr_to_review,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
